package com.sda.dp.game.model;

import java.awt.*;

public class CircleObject extends AbstractGameObject{

    @Override
    public void hit() {

    }

    public CircleObject(int x, int y) {
        super(new Point(x, y));
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        Color currentColor = g2d.getColor();

        // set color
        g2d.setColor(Color.RED);
        // paint something
        g2d.fillOval(position.x, position.y, 50, 50);

        // set last color
        g2d.setColor(currentColor);
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int getWidth() {
        return 0;
    }
}
