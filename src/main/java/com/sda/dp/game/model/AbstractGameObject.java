package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;
import com.sda.dp.game.VerticalDirection;

import java.awt.*;

public abstract class AbstractGameObject {
    protected Point position;
    protected HorizontalDirection hDir;
    protected VerticalDirection vDir;
    protected boolean toBeRemoved;


    protected double speed = 10;

    public AbstractGameObject(Point position) {
        this.position = position;
    }

    public AbstractGameObject() {
        this.position = new Point(100, 100);
    }

    public void move(double step) {
        if (hDir == HorizontalDirection.RIGHT) {
            position.x += speed * step;
        } else if (hDir == HorizontalDirection.LEFT) {
            position.x -= speed * step;
        }

        if (vDir == VerticalDirection.UP) {
            position.y -= speed * step;
        } else if (vDir == VerticalDirection.DOWN) {
            position.y += speed * step;
        }
    }

    public void paint(Graphics2D g2d){
        //move();
    }

    public Point getPosition() {
        return position;
    }

    public int getPositionY(){
        return position.y;
    }

    public int getPositionX(){
        return position.x;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void setPositionX(int x) {
        this.position.x = x;
    }

    public void setPositionY(int y) {
        this.position.y = y;
    }

    public void moveDown(){
        vDir = VerticalDirection.DOWN;
    }

    public void moveUp() {
        vDir = VerticalDirection.UP;
    }

    public void moveLeft() {
        hDir = HorizontalDirection.LEFT;
    }

    public void moveRight() {
        hDir = HorizontalDirection.RIGHT;
    }

    public void stopDown() {
        vDir = VerticalDirection.NONE;
    }

    public void stopUp() {
        vDir = VerticalDirection.NONE;
    }

    public void stopLeft() {
        hDir = HorizontalDirection.NONE;
    }

    public void stopRight() {
        hDir = HorizontalDirection.NONE;
    }

   public boolean checkCollision (AbstractGameObject strzal) {
    int posX = position.x;
    int posY = position.y;
    int posXEnd = position.x + getWidth();
    int posYEnd = position.y + getHeight();

    // gora, dol, prawo, lewo
    int fireY = strzal.position.y;
    int fireX = strzal.position.x;
    int fireYEnd = strzal.position.y + strzal.getHeight();
    int fireXEnd = strzal.position.x + strzal.getWidth();

    // czy górna krawędz jest pomiędzy górną a dolną krawędzią obiektu
    // czy dolan krawędz jest pomiędzy górną a dolną krawędzią obiektu
    if (fireY > posY && fireY < posYEnd || fireYEnd > posY && fireY < posYEnd) {

        if (fireX > posX && fireX < posXEnd || fireXEnd > posX && fireXEnd < posXEnd ) {
            return true;
        }
    }
    return false;
   }

    public abstract int getHeight();

    public abstract int getWidth();

    public boolean isToBeRemoved() {
        return toBeRemoved;
    }

    public void setToBeRemoved(boolean toBeRemoved){
        this.toBeRemoved = toBeRemoved;
    }

    public abstract void hit();
}
