package com.sda.dp.game.model;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.dispatcher.HeroFiredEvent;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Heart extends AbstractGameObject {

    private BufferedImage image;

    public Heart() {
        try {
            image = ImageIO.read(new File("src/main/resources/HeartSuperSmall.png"));
        } catch (IOException ex) {
            // handle exception...
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        // nie potrzebne bo klasa nadrzedna nic nie robi w tej metodzie
        //super.paint(g2d);

        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        //return 0;
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        //return 0;
        return image.getWidth();
    }

    public void paint(Graphics2D g2d, int i) {
        position.x = i*getWidth()+ i*5 ;
        position.y = 0;
        paint(g2d);
    }

    public void hit(){}
}
