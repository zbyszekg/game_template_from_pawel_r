package com.sda.dp.game.model;

import com.sda.dp.game.VerticalDirection;

import java.awt.*;

public class Fire extends AbstractGameObject {
    private Color fireColor;

    public Fire(int x, int y, double speed, boolean up) {
        super(new Point(x, y));
        this.speed = speed;

        if (up) {
            vDir = VerticalDirection.UP;
            fireColor = Color.RED;
        } else {
            vDir = VerticalDirection.DOWN;
            fireColor = Color.BLUE;
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        Color lastColor = g2d.getColor();

        g2d.setColor(fireColor);
        g2d.fillRect(position.x, position.y, 10,30);
        g2d.setColor(lastColor);
    }

    @Override
    public int getHeight() {
        return 30;
    }

    @Override
    public int getWidth() {
        return 10;
    }

    @Override
    public void hit() {

    }
}
