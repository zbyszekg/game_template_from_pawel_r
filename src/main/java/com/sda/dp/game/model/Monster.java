package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class Monster extends AbstractGameObject {

    private BufferedImage image; // onrazek potwora
    private int hp; // wytrzymałość na ciosy

    private int moveRange;
    private Point initialPosition;

    public Monster(int x, int y, int durability, int range) {
        super(new Point(x,y));
        this.initialPosition = new Point(x,y); // save starting position
        this.moveRange = range;
        this.hp = durability;
        try {
            image = ImageIO.read(new File("src/main/resources/enemy2Small.png"));
        } catch (IOException ex) {
            // handle exception...
            System.err.println("NIe mogłem załadować obrazka");
        }
        speed = 1.0;
        hDir = HorizontalDirection.RIGHT;
    }

    @Override
    public void move(double move) {
        // różnica pozycji
        int differenceX = position.x - initialPosition.x;
        if (differenceX > moveRange) {
            hDir = HorizontalDirection.LEFT;
            // odwracamy kierunek ruchu
        } else if (differenceX < 0) {
            hDir = HorizontalDirection.RIGHT;
        }
        super.move(move);
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {
        hp --;
        if (hp == 0) {
            toBeRemoved = true;
        }
    }
}
