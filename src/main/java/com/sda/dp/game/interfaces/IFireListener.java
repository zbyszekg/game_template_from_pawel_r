package com.sda.dp.game.interfaces;

public interface IFireListener {
    void addFire(int posX, int pozY, double speed, boolean up);
}